var merge = require('webpack-merge')
var publicEnv = require('./public.env')

module.exports = merge(publicEnv, {
  NODE_ENV: '"local"',
  ENV: '"local"',
  ecalibrationUrl: '"http://127.0.0.1:8088/ECalibrationApi/"',
  FrameWorkApiUrl: '"http://10.205.139.1/FrameWorkApi/"',
  UserManagementApiUrl: '"http://10.205.138.135:8080/UserManagementApi/"',
  CustomerApiUrl: '"http://10.205.138.135:8080/CustomerApi/"'
  //uat环境
  // ecalibrationUrl: '"http://test.sgsonline.com.cn/ECalibrationApi/"',
  // FrameWorkApiUrl: '"http://test.sgsonline.com.cn/FrameWorkApi/"',
  // UserManagementApiUrl: '"http://test.sgsonline.com.cn/UserManagementApi/"',
  // CustomerApiUrl: '"http://test.sgsonline.com.cn/CustomerApi/"'
})
