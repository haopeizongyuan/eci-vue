var merge = require('webpack-merge')
var publicEnv = require('./public.env')

module.exports = merge(publicEnv, {
  NODE_ENV: '"uat"',
  ENV: '"uat"',
  ecalibrationUrl: '"http://test.sgsonline.com.cn/ECalibrationApi/"',
  FrameWorkApiUrl: '"http://test.sgsonline.com.cn/FrameWorkApi/"',
  UserManagementApiUrl: '"http://test.sgsonline.com.cn/UserManagementApi/"',
  CustomerApiUrl: '"http://test.sgsonline.com.cn/CustomerApi/"'
})
