var merge = require('webpack-merge')
var publicEnv = require('./public.env')

module.exports = merge(publicEnv, {
  NODE_ENV: '"production"',
  ENV : '"prod"',
  ecalibrationUrl: '"http://www.sgsonline.com.cn/ECalibrationApi/"',
  FrameWorkApiUrl: '"http://www.sgsonline.com.cn/FrameWorkApi/"',
  UserManagementApiUrl: '"http://www.sgsonline.com.cn/UserManagementApi/"',
  CustomerApiUrl: '"http://www.sgsonline.com.cn/CustomerApi/"'
})
