var merge = require('webpack-merge')
var publicEnv = require('./public.env')

module.exports = merge(publicEnv, {
  NODE_ENV: '"development"',
  ENV : '"dev"',
  serviceUrl : '"http://10.205.139.111/start/"',
  ecalibrationUrl:'"http://10.205.138.148:8088/ECalibrationApi/"',
  FrameWorkApiUrl:'"http://10.205.139.111/FrameWorkApi/"'
})
