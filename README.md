``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run local  本地环境接口

# serve with hot reload at localhost:8080
npm run dev  uat环境接口

# build for dev with minification
npm run build:dev

# build for uat with minification
npm run build:uat

# build for prod with minification
npm run build:prod
