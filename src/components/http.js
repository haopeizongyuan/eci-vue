var axios = require('axios');
var util = require('./util.js');
var Promise = require('Promise');

module.exports = {
  post: function (_scope, url, paramJson, callback, conf) {
    if (!conf) {
      conf = {
        showLoading: false, //loading,加载中
        timeout: 15000, //请求超时
        showNotifyWhenSuccess: false //接口调用成功时， 是否显示操作成功提示
      };
    }
    this._execute('post', _scope, url, paramJson, callback, conf);
  },
  get: function (_scope, url, paramJson, callback, conf) {
    this._execute('get', _scope, url, paramJson, callback, conf);
  },
  _execute: function (method, _scope, url, paramJson, callback, conf) {
    var loading = null;
    if (conf.showLoading) {
      loading = _scope.$loading({
        lock: false,
        text: '加载中，请稍后...',
        background: 'rgba(0, 0, 0, 0.2)'
      });
    }
    var params;
    if(method=='post'){
      params = {sgsToken:util.getCookie('sgsToken')};
    }else{
      params = $.extend(null,paramJson,{sgsToken:util.getCookie('sgsToken')});
    }
    console.log("axios get params !");
    console.log(params);
    axios({
      method: method,
      url: url,
      data: paramJson,
      dataType: "json",
      timeout: conf.timeout ? conf.timeout : 15000,
      headers: {
        'Content-Type': 'application/json'
      },
      params: params,
    }).then(function (response) {
      if (response.data.errorCode != null) {
        _scope.$notify.error({
          title: '错误',
          message: '操作失败 : ' + response.data.errorCode + '|' + response.data.errorMessage
        });
        console.log(response.data);
      } else {
        if (conf.isCallback) {//如果需要回调函数，则调用
          callback(response.data);
        }


        if (conf.showNotifyWhenSuccess) {
          _scope.$notify({
            title: '成功',
            message: '操作成功!',
            type: 'success'
          });
        }
        if (conf.isReload) {//是否重新加载页面
          setTimeout(() => {
            location.reload(true);
          }, 2000);
        }
      }

      if (loading) {
        loading.close();
      }
    })
      .catch(function (error) {
        if (loading) {
          loading.close();
        }
        if (error && error.message) {
          console.info(url);
          _scope.$notify.error({
            title: '错误',
            message: ' : ' + error.message
          });
        } else {
          _scope.$notify.error({
            title: '错误',
            message: '系统异常，请与管理员联系!'
          });
        }

        // Promise.reject(error);
      });
  }
};
