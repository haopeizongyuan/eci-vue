Date.prototype.Format = function (fmt) { //author: meizz
  var o = {
    "M+": this.getMonth() + 1, //月份
    "d+": this.getDate(), //日
    "h+": this.getHours(), //小时
    "m+": this.getMinutes(), //分
    "s+": this.getSeconds(), //秒
    "q+": Math.floor((this.getMonth() + 3) / 3), //季度
    "S": this.getMilliseconds() //毫秒
  };
  if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
  for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
  return fmt;
}

module.exports = {
  getCookie: function (c_name) {
    if (document.cookie.length > 0) {
      let c_start = document.cookie.indexOf(c_name + "=")
      if (c_start != -1) {
        c_start = c_start + c_name.length + 1
        let c_end = document.cookie.indexOf(";", c_start)
        if (c_end == -1) c_end = document.cookie.length
        return unescape(document.cookie.substring(c_start, c_end))
      }
    }
    return "";
  },
  formatDateByFormat: function (date, fmt) {
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    let o = {
      'M+': date.getMonth() + 1,
      'd+': date.getDate(),
      'h+': date.getHours(),
      'm+': date.getMinutes(),
      's+': date.getSeconds()
    };
    for (let k in o) {
      if (new RegExp(`(${k})`).test(fmt)) {
        let str = o[k] + '';
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? str : this.padLeftZero(str));
      }
    }
    return fmt;
  },
  formatDate: function (date) {
    return this.formatDateByFormat(date, 'yyyy-MM-dd hh:mm:ss');
  },
  padLeftZero (str) {
    return ('00' + str).substr(str.length);
  },
  //日期格式化
  parseDate: function (str) {
    var s = str.replace(/-/g, "/");
    var date = new Date(s);
    return date;
  },
  goOrderDetail: function (type, id, orderno) {
    if (type == 1) {
      window.open("orderDetailFirststep.html?orderid=" + id + "&&orderno=" + orderno);
    }
    if (type == 2) {
      window.open("orderDetailSGSConfirm.html?orderid=" + id + "&&orderno=" + orderno);
    }
    if (type == 3) {
      window.open("orderDetailSchedule.html?orderid=" + id + "&&orderno=" + orderno);
    }
    if (type == 4) {
      window.open("orderDetailWorking.html?orderid=" + id + "&&orderno=" + orderno);
    }
    if (type == 5) {
      window.open("orderDetailFinish.html?orderid=" + id + "&&orderno=" + orderno);
    }
    if (type == 6) {
      window.open("orderDetailClosed.html?orderid=" + id + "&&orderno=" + orderno);
    }
  },
  toEquipDetail: function (id,isNewWindow) {
    isNewWindow = isNewWindow==null || isNewWindow ==""?false:true;
    if(isNewWindow){
      window.open("equDetail.html?id=" + id + "&isNew=false");
    }else{
      window.location.href = "equDetail.html?id=" + id + "&isNew=false";
    }
  },
  history: {
    /**
     * 浏览器后退
     */
    back () {
      window.history.back();
    },
    push (path) {
      window.location.href = path
    }
  }
}
