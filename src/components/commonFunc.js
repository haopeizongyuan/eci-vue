/*
 * 公共功能维护JS
 */

// 扩展Date属性，支持js日期格式化
Date.prototype.Format = function (fmt) {
  var o = {
    "M+": this.getMonth() + 1,
    "d+": this.getDate(),
    "h+": this.getHours(),
    "m+": this.getMinutes(),
    "s+": this.getSeconds(),
    "q+": Math.floor((this.getMonth() + 3) / 3),
    "S": this.getMilliseconds()
  };
  if (/(y+)/.test(fmt))
    fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
      .substr(4 - RegExp.$1.length));
  for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt))
      fmt = fmt.replace(RegExp.$1,
        (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k])
          .substr(("" + o[k]).length)));
  return fmt;
};

var core = {
  //下拉框动态取值数组转换
  copyArray: function (dataObj) {
    var arry = [];
    for (var key in dataObj) {
      var data = {};
      data.code = key;
      data.value = dataObj[key];
      arry.push(data);
    }
    return arry;
  },
  //日期格式化
  getDateFormat: function (str, format) {
    if (null == str || str == "") {
      return "";
    }
    if (null != format && format != '') {
      return new Date(Date.parse(
        str.replace(/-/g, "/"))).Format(format);
    } else {
      return new Date(Date.parse(
        str.replace(/-/g, "/"))).Format("yyyy-MM-dd");
    }
  },
  formatDate: function (date, fmt) {
    if (/(y+)/.test(fmt)) {
      fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    let o = {
      'M+': date.getMonth() + 1,
      'd+': date.getDate(),
      'h+': date.getHours(),
      'm+': date.getMinutes(),
      's+': date.getSeconds()
    };
    for (let k in o) {
      if (new RegExp(`(${k})`).test(fmt)) {
        let str = o[k] + '';
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? str : ('00' + str).substr(str.length));
      }
    }
    return fmt;
  },
  // 获取URL中传递的参数
  getUrlParam: function (object) {
    var reg = new RegExp("(^|&)" + object + "=([^&]*)(&|$)");
    if (window.location.href
      && (window.location.href.split("html?").length > 1)) {
      var r = window.location.href.split("html?")[1].match(reg)
      if (r != null) {
        return decodeURI(r[2]);
      } else {
        return "";
      }
    } else {
      return ""
    }
    ;
  },
  /**
   * 如URL中含有锚点值，则使用此方法获取参数
   * 获取url参数值
   * @method getUrlParam
   * @param {String} paramName 参数名
   * @return {String} 参数值
   */
  getUrlParam1: function (paramName) {
    var href = window.location.href;
    var url = decodeURI(href);
    var idx = url.indexOf("?");
    var params = url.substring(idx + 1);
    if (params) {
      params = params.split("&");
      for (var i = 0; i < params.length; i += 1) {
        var param = params[i].split("=");
        if (param[0] == paramName) {
          //完善获取url参数的逻辑
          var pArr = [];
          for (var k = 1, len = param.length; k < len; k++) {
            pArr.push(param[k]);
          }
          var p = pArr.join('=');
          var idx1 = p.indexOf("#");
          if (idx1 != -1) {
            p = p.substring(0, idx1);
          }
          return p;
        }
      }
    }
  },
  //替换指定传入参数的值,paramName为参数,replaceWith为新值
  replaceParamVal: function (paramName, replaceWith) {
    var oUrl = window.location.href.toString();
    var re = eval('/(' + paramName + '=)([^&]*)/gi');
    var nUrl = oUrl.replace(re, paramName + '=' + replaceWith);
    window.history.pushState({}, 0, nUrl);
  },
  getsec: function (str) {
    var str1 = str.substring(1, str.length) * 1;
    var str2 = str.substring(0, 1);
    if (str2 == "s") {
      return str1 * 1000;
    }
    else if (str2 == "h") {
      return str1 * 60 * 60 * 1000;
    }
    else if (str2 == "d") {
      return str1 * 24 * 60 * 60 * 1000;
    }
  },
  setCookie: function (name, value, time) {
    var strsec = this.getsec(time);
    var exp = new Date();
    exp.setTime(exp.getTime() + strsec * 1);
    document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString();
  },
  getCookie: function (name) {
    var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
    if (arr = document.cookie.match(reg))
      return unescape(arr[2]);
    else
      return null;
  },
  checkCookie: function (name) {
    var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
    if (arr = document.cookie.match(reg))
      return true;
    else
      return false;
  },
  /**
   * 把字符串转换成Date，如果是Date类型，则不转换。2016-8-31
   * @param date
   * @returns {Date}
   */
  dateToDate: function (date) {
    var sDate = new Date();
    if (typeof date == "string") {
      var arr = date.split('-');
      if (arr.length == 3) {
        sDate = new Date(arr[0] + '-' + arr[1] + '-' + arr[2]);
      }
    } else {
      sDate = date;
    }
    return sDate;
  },
  /**
   * 日期加上任意天数或减去任意天数
   * @param date
   * @param dayCount
   * @returns {Date}
   */
  dateAddDate: function (date, dayCount) {
    var tempDate = this.dateToDate(date);
    var count = parseInt(dayCount);
    var oldYear = tempDate.getFullYear();
    var oldMonth = tempDate.getMonth();
    var oldDate = tempDate.getDate();
    var newDate = oldDate + count;
    return new Date(oldYear, oldMonth, newDate);
  }, /**
   * 日期加上任意月份
   * @param date
   * @param monthCount
   * @returns {Date}
   */
  dateAddMonth: function (date, monthCount) {
    var tempDate = this.dateToDate(date);
    var count = parseInt(monthCount);
    if (count < 0) {
      return new Date();
    }
    var oldYear = tempDate.getFullYear();
    var oldMonth = tempDate.getMonth();
    var oldDate = tempDate.getDate();
    var newMonth = oldMonth + count;
    var newDate = new Date(oldYear, newMonth, oldDate);
    //防止月份数不一致，进行微调
    while (newDate.getMonth() != (newMonth % 12)) {
      oldDate--;
      newDate = new Date(oldYear, newMonth, oldDate);
    }
    return newDate;
  }, /**
   * 日期加减年
   * @param date
   * @param yearCount
   * @returns {Date}
   */
  dateAddYear: function (date, yearCount) {
    var tempDate = this.dateToDate(date);
    var count = parseInt(yearCount);
    var oldYear = tempDate.getFullYear();
    var oldMonth = tempDate.getMonth();
    var oldDate = tempDate.getDate();
    var newYear = oldYear + count;
    var newDate = new Date(newYear, oldMonth, oldDate);
    //防止月份数不一致，进行微调
    while (newDate.getMonth() != oldMonth) {
      oldDate--;
      newDate = new Date(newYear, oldMonth, oldDate);
    }
    return newDate;
  },
  countdown (second = 60, callback) {
    if (typeof second !== 'number') {
      return;
    }
    var _this = this;
    var s = '', time
    if (second == 0) {
      callback && callback(0)
    } else {
      time = setTimeout(function () {
        s = second - 1
        callback && callback(s)
        _this.countdown(s, callback);
      }, 1000)
    }
  }
}

module.exports = core
