// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import orderSubmit from './orderSubmit.vue'

import ElementUI from 'element-ui'
import '../../../static/css/elementui-theme/index.css'
import 'iview/dist/styles/iview.css'
import '../../../static/css/sgs_style.css'
/*Vue.use(preOrderComponent)*/
import 'babel-polyfill'

window.Promise = Promise
window.eventBus = new Vue();

Vue.use(ElementUI, {size: 'small'})

console.log(process.env);
// Vue.setEnv(process.env);

Vue.config.errorHandler = function (err, vm) {
  console.error('Vue Error Handler', err, vm);
}

new Vue({
  el: '#app',
  render: h => h(orderSubmit)
});
