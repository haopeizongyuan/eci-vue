var path = require('path')
var config = require('../config')
var ExtractTextPlugin = require('extract-text-webpack-plugin')
var glob = require('glob');

exports.assetsPath = function (_path) {
  var assetsSubDirectory = process.env.NODE_ENV === 'production'
    ? config.build.assetsSubDirectory
    : config.local.assetsSubDirectory
    
    console.log('assetPath:' + path.posix.join(assetsSubDirectory, _path));
  return path.posix.join(assetsSubDirectory, _path)
}

exports.cssLoaders = function (options) {
  options = options || {}

  var cssLoader = {
    loader: 'css-loader',
    options: {
      minimize: process.env.NODE_ENV === 'production',
      sourceMap: options.sourceMap
    }
  }

  // generate loader string to be used with extract text plugin
  function generateLoaders (loader, loaderOptions) {
    var loaders = [cssLoader]
    if (loader) {
      loaders.push({
        loader: loader + '-loader',
        options: Object.assign({}, loaderOptions, {
          sourceMap: options.sourceMap
        })
      })
    }

    // Extract CSS when that option is specified
    // (which is the case during production build)
    if (options.extract) {
      return ExtractTextPlugin.extract({
        use: loaders,
        fallback: 'vue-style-loader',
        publicPath: '../../'
      })
    } else {
      return ['vue-style-loader'].concat(loaders)
    }
  }

  // https://vue-loader.vuejs.org/en/configurations/extract-css.html
  return {
    css: generateLoaders(),
    postcss: generateLoaders(),
    less: generateLoaders('less'),
    sass: generateLoaders('sass', { indentedSyntax: true }),
    scss: generateLoaders('sass'),
    stylus: generateLoaders('stylus'),
    styl: generateLoaders('stylus')
  }
}

// Generate loaders for standalone style files (outside of .vue)
exports.styleLoaders = function (options) {
  var output = []
  var loaders = exports.cssLoaders(options)
  for (var extension in loaders) {
    var loader = loaders[extension]
    output.push({
      test: new RegExp('\\.' + extension + '$'),
      use: loader
    })
  }
  return output
}

//按照开发结构规范获取所有入口文件
exports.getEntries = function(globPath) {
  var entries = {}
  var allmatches = glob.sync(globPath);
  allmatches = allmatches.sort();
  console.log('=======================' + globPath + '============================')
  allmatches.forEach(function(entry) {
      var tmp = entry.split('/')
      if(tmp.length > 5) {
		var moduleName = tmp[4]
		var fileName = tmp[5].split('.')[0]
		if (moduleName.toLowerCase() === fileName.toLowerCase()) {
			entries[moduleName] = entry
			console.log('entries push....', moduleName, entry)
		}
	  } else {
		var moduleName = tmp[3]
		var fileName = tmp[4].split('.')[0]
		if (moduleName.toLowerCase() === fileName.toLowerCase()) {
			entries[moduleName] = entry
			console.log('entries push....', moduleName, entry)
		}
	  }
  });
  return entries;
}

exports.envInfo = function () {
  var argv = process.argv
  if (argv.indexOf("uat")>-1) {
    return "uat"; 
  } else if (argv.indexOf("dev")>-1) {
	  return "dev"; 
  } else if (argv.indexOf("prod")>-1) {
	  return "prod"; 
  }
}
